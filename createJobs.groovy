pipelineJob('pipelineJob') {
    definition {
        cps {
            script(readFileFromWorkspace('pipelineJob.groovy'))
            sandbox()
        }
    }
}
pipelineJob('multi-media-job') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url 'https://bitbucket.org/mjmacaraeg/multimedia-services.git'
                    }
                    branch 'developer'
                }
            }
        }
    }
}